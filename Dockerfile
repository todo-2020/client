FROM node:lts-alpine AS BUILD_IMAGE
RUN apk update && apk add curl bash && rm -rf /var/cache/apk/*
RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
# build app for production with minification
RUN npm run build
RUN npm prune --production
RUN /usr/local/bin/node-prune
# production environment
FROM nginx:latest
RUN apt update && apt install curl  -y
WORKDIR /app
COPY ./k8s/nginx/nginx.conf /etc/nginx/
COPY ui-tests ./ui-tests
COPY --from=BUILD_IMAGE /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
 