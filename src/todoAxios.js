// axios
import axios from "axios";

const domain = process.env.VUE_APP_API_URL
const api_version  = process.env.VUE_APP_API_VERSION
console.log(process.env)
export default axios.create({
  baseURL: domain + api_version,
  timeout: 4000,
});
