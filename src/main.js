import Vue from "vue";
import App from "./App.vue";
import VueTailwind from "vue-tailwind";
import config from '../config'
import "./assets/styles/main.css";



Vue.config.productionTip = false;

Vue.use(VueTailwind);
Vue.mixin({
  data: function() {
    return {
      globalVar: config.VUE_APP_API_URL,
    };
  },
});
new Vue({
  render: (h) => h(App),
}).$mount("#app");
