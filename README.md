
# ToDo Client

This project is consuming Todo-Server Api and present simple UI for adding, updating and deleting todos.


### Platforms 

`Linux 5.4.0-31-generic GNU/Linux`

#### Build With

- VueJs

## UI Test Screen Record

![](public/gauge-tests.gif)
