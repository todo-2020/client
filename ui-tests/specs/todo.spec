# Getting Started Todo Interview UI Test

This is an executable specification file for todo client app. Every item given in this list will be running before prod delivery. To run these specs use below command:
	npm test


## Add task to the empthy list
// __Given__ Empty ToDo list
// __When__ I write "buy some milk" to ___text box___ and click to ___add button___
// __Then__ I should see "buy some milk" item in ToDo list

* Open "Todos" page
* Add "buy some milk"
* "buy some milk" exist in todo list

## Add task to the existing list

// __Given__ ToDo list with "buy some milk" item
// __When__ I write "enjoy the assignment" to ___text box___ and click to ___add button___
// __Then__ I should see "enjoy the assignment" item in ToDo list below "buy some milk"
* Open "Todos" page
* Add "buy some milk"
* Add "enjoy the assignment"
* "enjoy the assignment" exist in todo list
* Verify todo list
   |todo                    |
   |------------------------|
   |buy some milk			    |
   |enjoy the assignment    |
## Mark done todo item

// __Given__ ToDo list with "buy some milk" item
// __When__  I click on "buy some milk" text
// __Then__ I should see "buy some milk" item marked as ~~"buy some milk"~~
* Open "Todos" page
* Add "buy some milk"
* Click on the the todo item "buy some milk"
* Verify mark for "buy some milk"

## Remove done mark

// __Given__ ToDo list with marked "buy some milk" item
// __When__ I click on "buy some milk" text
// __Then__ I should see mark of ~~"buy some milk"~~ item should be cleared as "buy some milk"
* Open "Todos" page
* Add "buy some milk"
* Click on the the todo item "buy some milk"
* Click on the the todo item "buy some milk"
* Verify unmark for "buy some milk"

## Delete item, check empty list

// * __Given__ ToDo list with "rest for a while" item
// * __When__ I click on ___delete button___ next to "rest for a while" item
// * __Then__ List should be empty
* Open "Todos" page
* Add "rest for a while"
* Click delete item "rest for a while"
* Verify empthy list

## Delete item, check list remainings

// __Given__ ToDo list with "rest for a while" and "drink water" item in order
// __When__ I click on ___delete button___ next to "rest for a while" item
// __Then__ I should see "drink water" item in ToDo list
* Open "Todos" page
* Add "rest for a while"
* Add "drink water"
* Click delete item "rest for a while"
* Verify remaining "drink water" with count "1"

