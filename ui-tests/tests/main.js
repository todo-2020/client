/* globals gauge*/
'use strict';
const {
  openBrowser,
  closeBrowser,
  click, text,toRightOf,
  storage
} = require('taiko');
const {localStorage, sessionStorage} = storage;
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
  await openBrowser({ headless: headless,args:['--no-sandbox', '--disable-setuid-sandbox'] });
});

beforeScenario(async (context) => {
  var hasItem = false
  do {
    hasItem =  await text("Delete").exists();
    if(hasItem){
      await click("Delete");
    }
  }while(hasItem)
});
afterScenario(async () => {
  await localStorage.clear();
});

afterSuite(async () => {
  // await closeBrowser();
});
