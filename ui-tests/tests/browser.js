const { goto } = require('taiko');

step("Open <page> page", async () => {
  console.log(process.env.SITE_URL)
  await goto(process.env.SITE_URL);
});
