/* globals gauge*/
"use strict";
const {
  text,
  below,
  textBox,
  $
} = require("taiko");

const assert = require("assert");

async function todoExists(todo) {
  return await text(todo, below(textBox({ placeholder: "Add Todo" }))).exists();
}

step("<todo> exist in todo list", async (todo) => {
  const exists = await todoExists(todo);
  assert.ok(exists);
});
step("Verify todo list <items>", async function(items) {
  var todos = []
  for(var i=0;i<items.rows.length-1; i++) {
      assert.ok(await text(items.rows[i+1].cells[0], below(text(items.rows[i].cells[0]))).exists());
  }
});
step("Verify mark for <todo>", async function(todo) {
    assert.strictEqual(await $('.line-through').text(),todo)
});
step("Verify unmark for <todo>", async function(todo) {
    assert.ok(!(await $('.line-through').elements())[0])
});
step("Verify empthy list", async function(){
    assert.ok(!(await $('.todo-item').elements())[0])
});

step("Verify remaining <todo> with count <count>", async function(todo,count) {
    assert.strictEqual(await $('.todo-item-text').text(),todo)
    var items = await $('.todo-item').elements()
    assert.strictEqual(items.length ,+count)
});



