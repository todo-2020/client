const { write, textBox, press, checkBox, click, text,toRightOf } = require("taiko");

step(
  "Type <todo> in to the input box <placeholder>",
  async (value, placeholder) => {
    await write(value, textBox({ placeholder }));
  }
);
step("Press <key> key", async (key) => {
  await press(key);
});

step("Click on the the todo item <todo>", async (todo) => {
  await click(text(todo));
});

step("Click delete item <todo>", async (todo) => {
  await click("Delete", toRightOf(todo));
});